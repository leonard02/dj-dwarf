<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use App\Http\Requests\BookingRequest;

class BookingController extends Controller
{
    public function book(BookingRequest $request)
    {
    	$full_name = $request->input('first_name')." ".$request->input('last_name');

    	Mail::send('email.booking',
	    array(
	        'name' => $full_name,
	        'email' => $request->input('email'),
	        'contact' => $request->input('contact'),
	        'user_message' => $request->input('message')
        ), function($message) use ($request) {
            $message->from($request->input('email'));
	        $message->to('leo@wb5.me', 'DJ DWARF')->subject('DJ DWARF BOOKING');
	    });

	    return redirect('/success');
    }
}
