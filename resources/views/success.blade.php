
<!doctype html>
<html class="no-js" lang="en_EN">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>DJ DW4RF</title>
        <meta name="description" content="DJ DW4RF">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<link href='/components/slick/slick.css' rel='stylesheet' type='text/css'>
		<link href='/components/swipebox/src/css/swipebox.min.css' rel='stylesheet' type='text/css'>
		<link href='/fonts/iconfont/style.css' rel='stylesheet' type='text/css'>
		<link href='/fonts/qticons/qticons.css' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="/css/qt-main.css"><!-- INCLUDES THE CSS FRAMEWORK VIA #IMPORT AND SASS -->
		<link rel="shortcut icon" type="image/png" href="/images/favicon.png"/>
	</head>
	<body class="qt-debug">
	<!-- QT HEADER END ================================ -->

		<div class="section section-message parallax-container qt-fullscreen scrollspy" >
			<div class="parallax"><img src="images/galaxy-1.jpg"></div>
			<div class="qt-polydecor poly1">
				<div class="qt-valign-wrapper">
					<div class="qt-valign">
						<div class="container">
							<div class="qt-framed center-align" >
									<div class="thankyou">
										<h2 class="qt-section-title">MESSAGE SENT<i class="deco"></i></h2>
										<h4>Thank you, we will answer as soon as possible</h4>
										<a href="/" class="btn"><span class="lnr lnr-home"></span>  Go Back</a>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div> 
		</div>

		<!-- QT FOOTER ================================ -->
		<script src="/js/modernizr-custom.js"></script>
		<script src="/js/jquery.js"></script><!--  JQUERY VERSION MUST MATCH WORDPRESS ACTUAL VERSION (NOW 1.12) -->
		<script src="/js/jquery-migrate.min.js"></script><!--  JQUERY VERSION MUST MATCH WORDPRESS ACTUAL VERSION (NOW 1.12) -->

		<!--  CUSTOM JS LIBRARIES: =========================================================== -->
		<script src="/js/materializecss/bin/materialize.min.js"></script>
		<script src="/components/sticky/jquery.sticky.js"></script>
		<script src="/components/slick/slick.min.js"></script>
		<script src="/components/particles/particles.min.js"></script>
		<script src="/components/skrollr/skrollr.min.js"></script>
		<script src="/components/swipebox/lib/ios-orientationchange-fix.js"></script>
		<script src="/components/swipebox/src/js/jquery.swipebox.min.js"></script>

		<!-- MAIN JAVASCRIPT FILE ================================ -->
		<script src="js/qt-main.js"></script>
	</body>
</html>