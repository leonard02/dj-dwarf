<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>DJ DW4RF</title>
        <meta name="description" content="DJ DW4RF">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link href='components/slick/slick.css' rel='stylesheet' type='text/css'>
        <link href='components/swipebox/src/css/swipebox.min.css' rel='stylesheet' type='text/css'>
        <link href='fonts/iconfont/style.css' rel='stylesheet' type='text/css'>
        <link href='fonts/qticons/qticons.css' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/qt-main.css"><!-- INCLUDES THE CSS FRAMEWORK VIA #IMPORT AND SASS -->
        <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>
    </head>
    <body class="qt-debug-disabled">
    <!-- QT HEADER END ================================ -->

            
    <!-- ====================== HEADER ================================================ -->
    <header id="impressium" class="qt-header parallax-container scrollspy" >
        <div class="parallax" data-start="width:100%;height:100%;top: 0%;left: 0%;" data-end="width:260%;height:260%;top:-80%;left:-80%;"><img src="images/bio.jpg" alt="background"></div>
        <div class="qt-valign-wrapper">
            <div class="qt-valign">
                <div class="container center-align"  >
                            <div class="qt-polydecor poly1">
                                <h3 style="color:#ff1744;">Angaben gemäß § 5 TMG:</h3>
                                <h4>Konstantin Schmelz<br />
                                KS-Booking<br />
                                FLEISCHMANNGASSE 7<br />
                                Top 13<br />
                                1040 Wien
                                </h4>
                                <h3 style="color:#ff1744;">Kontakt:</h3>
                                <h4>Telefon: +43 660 653 072 9</h4>
                                <h4>E-Mail: office@ks-booking.com</h4>
                            </div>
                </div>
            </div>
        </div>
        <div class="qt-particles" id="particlesheader" data-color="#ffffff" data-opacity="0.5" data-speed="1"></div>
    </header> 
    <!-- ====================== HEADER END ================================================ -->

            
    <!-- ====================== MENU ================================================ -->
     <div id="menu" class="qt-menu-wrapper" data-0-top>
        <nav id="stickymenu" class="qt-menu grey-text text-lighten-4"  >
            <!-- desktop menu -->
            <ul class="qt-desktopmenu">
                <li><a href="#impressium" class="qwsmoothscroll">Impressum</a></li>
                <li><a href="#disclaimer" class="qwsmoothscroll">Haftungsauschluss</a></li>
                <li><a href="#legal" class="qwsmoothscroll">Datenschutzerklärung</a></li>
            </ul>
            <!-- mobile menu -->
            <ul id="slide-out" class="side-nav qt-mobilemenu">
                <li><a href="#impressium" class="qwsmoothscroll">Impressum</a></li>
                <li><a href="#disclaimer" class="qwsmoothscroll">Haftungsauschluss</a></li>
                <li><a href="#legal" class="qwsmoothscroll">Datenschutzerklärung</a></li>
            </ul>
            <a href="#" data-activates="slide-out" class="button-collapse qt-menu-switch"><span class="lnr lnr-menu"></span></a>
        </nav>
    </div>
    <!-- ====================== MENU END ================================================ -->


            <!-- main section  ================================ -->
            <main class="qt-main">
                

                <div id="disclaimer" class="section section-video parallax-container qt-fullscreen scrollspy" >
                    <div class="parallax" ><img src="images/galaxy-1.jpg" alt="background"></div>
                    <div class="container center-align">
                        <div class="qt-framed flow-text"  data-100p-top="opacity:1;" data-80p-top="opacity:0;" data-30p-top="opacity:2;" >
                            <h2 class="qt-section-title">Haftungsauschluss<i class="deco"></i></h2>
                            <div class="content qt-polydecor poly3"  style="padding-left:30px;padding-right:30px;margin-bottom:100px;">
                            <h3 style="color:#ff1744;">Haftung für Inhalte</h3> <p>Als Diensteanbieter sind wir gemäß § 7
                            Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen
                            Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als
                            Diensteanbieter jedoch nicht verpflichtet, übermittelte oder
                            gespeicherte fremde Informationen zu überwachen oder nach Umständen zu
                            forschen, die auf eine rechtswidrige Tätigkeit hinweisen.</p>
                            <p>Verpflichtungen zur Entfernung oder Sperrung der Nutzung von
                            Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt.
                            Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der
                            Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von
                            entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend
                            entfernen.</p> 
                            <h3 style="color:#ff1744;">Haftung für Links</h3> <p>Unser Angebot enthält Links
                            zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss
                            haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr
                            übernehmen. Für die Inhalte der verlinkten Seiten ist stets der
                            jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die
                            verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche
                            Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der
                            Verlinkung nicht erkennbar.</p> <p>Eine permanente inhaltliche Kontrolle
                            der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer
                            Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von
                            Rechtsverletzungen werden wir derartige Links umgehend entfernen.</p>
                            <h3 style="color:#ff1744;">Urheberrecht</h3> <p>Die durch die Seitenbetreiber erstellten
                            Inhalte und Werke auf diesen Seiten unterliegen dem deutschen
                            Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede
                            Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der
                            schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers.
                            Downloads und Kopien dieser Seite sind nur für den privaten, nicht
                            kommerziellen Gebrauch gestattet.</p> <p>Soweit die Inhalte auf dieser
                            Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte
                            Dritter beachtet. Insbesondere werden Inhalte Dritter als solche
                            gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung
                            aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei
                            Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte
                            umgehend entfernen.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="legal" class="section section-legal qt-fullscreen scrollspy  qt-polydecor poly3">
                    <div class="qt-valign-wrapper">
                        <div class="qt-valign flow-text">
                            <h2 class="qt-section-title" data-100p-top="opacity:0;" data-70p-top="opacity:1;">Datenschutzerklärung<i class="deco"></i></h2>
                            <div class="qt-legal">
                                <h3 class="center-align" style="color:#ff1744;">Datenschutz</h3>
                                <p>Die Betreiber dieser Seiten nehmen den Schutz
                                    Ihrer persönlichen Daten sehr ernst. Wir behandeln Ihre
                                    personenbezogenen Daten vertraulich und entsprechend der gesetzlichen
                                    Datenschutzvorschriften sowie dieser Datenschutzerklärung.</p> <p>Die
                                    Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener
                                    Daten möglich. Soweit auf unseren Seiten personenbezogene Daten
                                    (beispielsweise Name, Anschrift oder E-Mail-Adressen) erhoben werden,
                                    erfolgt dies, soweit möglich, stets auf freiwilliger Basis. Diese Daten
                                    werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte
                                    weitergegeben.</p> <p>Wir weisen darauf hin, dass die Datenübertragung
                                    im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken
                                    aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch
                                    Dritte ist nicht möglich.</p><p> </p>
                                    <h3 class="center-align" style="color:#ff1744;">Datenschutzerklärung für die Nutzung von Google Analytics</h3>
                                    <p>Diese Website nutzt Funktionen des Webanalysedienstes Google
                                    Analytics. Anbieter ist die Google Inc., 1600 Amphitheatre Parkway
                                    Mountain View, CA 94043, USA.</p> <p>Google Analytics verwendet sog.
                                    "Cookies". Das sind Textdateien, die auf Ihrem Computer gespeichert
                                    werden und die eine Analyse der Benutzung der Website durch Sie
                                    ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre
                                    Benutzung dieser Website werden in der Regel an einen Server von Google
                                    in den USA übertragen und dort gespeichert.</p> <p>Mehr Informationen
                                    zum Umgang mit Nutzerdaten bei Google Analytics finden Sie in der
                                    Datenschutzerklärung von Google: <a
                                    href="https://support.google.com/analytics/answer/6004245?hl=de">https://support.google.com/analytics/answer/6004245?hl=de</a></p>
                                    <p><strong>Browser Plugin</strong></p> <p>Sie können die Speicherung der
                                    Cookies durch eine entsprechende Einstellung Ihrer Browser-Software
                                    verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall
                                    gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich
                                    werden nutzen können. Sie können darüber hinaus die Erfassung der durch
                                    das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten
                                    (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten
                                    durch Google verhindern, indem sie das unter dem folgenden Link
                                    verfügbare Browser-Plugin herunterladen und installieren: <a
                                    href="https://tools.google.com/dlpage/gaoptout?hl=de">https://tools.google.com/dlpage/gaoptout?hl=de</a></p>
                                    <p><strong>Widerspruch gegen Datenerfassung</strong></p> <p>Sie können
                                    die Erfassung Ihrer Daten durch Google Analytics verhindern, indem Sie
                                    auf folgenden Link klicken. Es wird ein Opt-Out-Cookie gesetzt, dass das
                                    Erfassung Ihrer Daten bei zukünftigen Besuchen dieser Website
                                    verhindert: <a href="javascript:gaOptout();">Google Analytics
                                    deaktivieren</a></p>
                                    <p><strong>IP-Anonymisierung</strong></p> <p>Wir nutzen die Funktion
                                    "Aktivierung der IP-Anonymisierung" auf dieser Webseite. Dadurch wird
                                    Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der
                                    Europäischen Union oder in anderen Vertragsstaaten des Abkommens über
                                    den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen
                                    wird die volle IP-Adresse an einen Server von Google in den USA
                                    übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website
                                    wird Google diese Informationen benutzen, um Ihre Nutzung der Website
                                    auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen
                                    und um weitere mit der Websitenutzung und der Internetnutzung verbundene
                                    Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im
                                    Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse
                                    wird nicht mit anderen Daten von Google zusammengeführt.</p>
                                    <h3 class="center-align" style="color:#ff1744;">Datenschutzerklärung für die Nutzung von Instagram</h3>
                                    <p>Auf unseren Seiten sind Funktionen des Dienstes Instagram
                                    eingebunden. Diese Funktionen werden angeboten durch die Instagram Inc.,
                                    1601 Willow Road, Menlo Park, CA, 94025, USA integriert. Wenn Sie in
                                    Ihrem Instagram - Account eingeloggt sind können Sie durch Anklicken des
                                    Instagram - Buttons die Inhalte unserer Seiten mit Ihrem Instagram -
                                    Profil verlinken. Dadurch kann Instagram den Besuch unserer Seiten Ihrem
                                    Benutzerkonto zuordnen. Wir weisen darauf hin, dass wir als Anbieter der
                                    Seiten keine Kenntnis vom Inhalt der übermittelten Daten sowie deren
                                    Nutzung durch Instagram erhalten.</p> <p>Weitere Informationen hierzu
                                    finden Sie in der Datenschutzerklärung von Instagram: <a
                                    href="http://instagram.com/about/legal/privacy/">http://instagram.com/about/legal/privacy/</a></p><p> </p>
                                    <h3 class="center-align" style="color:#ff1744;">Server-Log-Files</h3> <p>Der Provider der Seiten erhebt und
                                    speichert automatisch Informationen in so genannten Server-Log Files,
                                    die Ihr Browser automatisch an uns übermittelt. Dies sind:</p> <ul>
                                    <li>Browsertyp/ Browserversion</li> <li>verwendetes Betriebssystem</li>
                                    <li>Referrer URL</li> <li>Hostname des zugreifenden Rechners</li>
                                    <li>Uhrzeit der Serveranfrage</li> </ul> <p><br />Diese Daten sind nicht
                                    bestimmten Personen zuordenbar. Eine Zusammenführung dieser Daten mit
                                    anderen Datenquellen wird nicht vorgenommen. Wir behalten uns vor, diese
                                    Daten nachträglich zu prüfen, wenn uns konkrete Anhaltspunkte für eine
                                    rechtswidrige Nutzung bekannt werden.</p><p> </p>
                                    <h3 class="center-align" style="color:#ff1744;">Kontaktformular</h3> <p>Wenn Sie uns per Kontaktformular Anfragen
                                    zukommen lassen, werden Ihre Angaben aus dem Anfrageformular inklusive
                                    der von Ihnen dort angegebenen Kontaktdaten zwecks Bearbeitung der
                                    Anfrage und für den Fall von Anschlussfragen bei uns gespeichert. Diese
                                    Daten geben wir nicht ohne Ihre Einwilligung weiter.</p><p> </p>
                                    <h3 class="center-align" style="color:#ff1744;">Newsletterdaten</h3> <p>Wenn Sie den auf der Webseite angebotenen
                                    Newsletter beziehen möchten, benötigen wir von Ihnen eine E-Mail-Adresse
                                    sowie Informationen, welche uns die überprüfung gestatten, dass Sie der
                                    Inhaber der angegebenen E-Mail-Adresse sind und mit dem Empfang des
                                    Newsletters einverstanden sind. Weitere Daten werden nicht erhoben.
                                    Diese Daten verwenden wir ausschließlich für den Versand der
                                    angeforderten Informationen und geben sie nicht an Dritte weiter.</p>
                                    <p>Die erteilte Einwilligung zur Speicherung der Daten, der
                                    E-Mail-Adresse sowie deren Nutzung zum Versand des Newsletters können
                                    Sie jederzeit widerrufen , etwa über den „Austragen“-Link im
                                    Newsletter.</p>
                            </div>
                        </div>
                    </div>
                </div>

            </main>
            <!-- main section end ================================ -->

            <!-- footer section  ================================ -->
            
    <!-- ====================== FOOTER ================================================ -->
    <footer class="qt-footer parallax-container blue-grey-text text-lighten-5">
        <div class="parallax"><img src="images/footer.jpg" alt="background"></div>
        <div class="container center-align"  >
            <h3 class="qt-footertitle"><img src="images/logo-footer.png" alt="DJ DW4RF"></h3>
            <h5>KS Booking, Inh. Konstantin Schmelz, Fleischmanngasse 7/13, Wien, Österreich</h5>
            <p class="qt-social">
                <a href="http://www.mixcloud.com/djdw4rf" target="_blank"><span class="qticon-mixcloud"></span></a>
                <a href="https://www.facebook.com/profile.php?id=291386711072310&fref=ts" target="_blank"><span class="qticon-facebook"></span></a>
                <a href="https://www.instagram.com/djdw4rf/" target="_blank"><span class="qticon-instagram"></span></a>
                <a href="https://www.youtube.com/channel/UCaXZFqw0OiucUz9zbuEvyfQ" target="_blank"><span class="qticon-youtube"></span></a>
                <a href="http://www.soundcloud.com/djdw4rf" target="_blank"><span class="qticon-soundcloud"></span></a>
            </p>
            <h5><a href="{{ url('/') }}">Home</a> | <a href="{{ url('/legal') }}">Imprint</a></h5>
        </div>
        <div class="qt-particles" id="particlesfooter" data-color="#ffffff" data-opacity="0.2" data-speed="1"></div>
    </footer>
    <div class="qt-firefoxfix"></div>
    <!-- ====================== FOOTER END ================================================ -->
        <!-- footer section end  ================================ -->

        <!-- QT FOOTER ================================ -->
        <script src="js/modernizr-custom.js"></script>
        <script src="js/jquery.js"></script><!--  JQUERY VERSION MUST MATCH WORDPRESS ACTUAL VERSION (NOW 1.12) -->
        <script src="js/jquery-migrate.min.js"></script><!--  JQUERY VERSION MUST MATCH WORDPRESS ACTUAL VERSION (NOW 1.12) -->

        <!--  CUSTOM JS LIBRARIES: =========================================================== -->
        <script src="js/materializecss/bin/materialize.min.js"></script>
        <script src="components/slick/slick.min.js"></script>
        <script src="components/skrollr/skrollr.min.js"></script>
        <script src="components/particles/particles.min.js"></script>
        
        <script src="components/swipebox/lib/ios-orientationchange-fix.js"></script>
        <script src="components/swipebox/src/js/jquery.swipebox.min.js"></script>
        
        <!-- MAIN JAVASCRIPT FILE ================================ -->
        <script src="js/qt-main.js"></script>

    </body>
</html>