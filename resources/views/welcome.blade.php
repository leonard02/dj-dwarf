<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>DJ DW4RF</title>
        <meta name="description" content="DJ DW4RF">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link href='components/slick/slick.css' rel='stylesheet' type='text/css'>
        <link href='components/swipebox/src/css/swipebox.min.css' rel='stylesheet' type='text/css'>
        <link href='fonts/iconfont/style.css' rel='stylesheet' type='text/css'>
        <link href='fonts/qticons/qticons.css' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/qt-main.css"><!-- INCLUDES THE CSS FRAMEWORK VIA #IMPORT AND SASS -->
        <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>
    </head>
    <body class="qt-debug-disabled">
    <!-- QT HEADER END ================================ -->

            
    <!-- ====================== HEADER ================================================ -->
    <header id="home" class="qt-header parallax-container scrollspy" >
        <div class="parallax" data-start="width:100%;height:100%;top: 0%;left: 0%;" data-end="width:260%;height:260%;top:-80%;left:-80%;"><img src="images/bio.jpg" alt="background"></div>
        <div class="qt-valign-wrapper">
            <div class="qt-valign">
                <div class="container center-align"  >
                    <img src="images/logo.png" class="logo" alt="DJ DW4RF">
                </div>
            </div>
        </div>
        <div class="qt-particles" id="particlesheader" data-color="#ffffff" data-opacity="0.5" data-speed="1"></div>
    </header> 
    <!-- ====================== HEADER END ================================================ -->

            
    <!-- ====================== MENU ================================================ -->
    <div id="menu" class="qt-menu-wrapper" data-0-top>
        <nav id="stickymenu" class="qt-menu grey-text text-lighten-4"  >
            <!-- desktop menu -->
            <ul class="qt-desktopmenu">
                <li><a href="#home" class="qwsmoothscroll">Home</a></li>
                <li><a href="#photos" class="qwsmoothscroll">Photos</a></li>
                <li><a href="#bio" class="qwsmoothscroll">Bio</a></li>
                <li><a href="#tours" class="qwsmoothscroll">Tours</a></li>
                <li><a href="#booking" class="qwsmoothscroll">Booking</a></li>
            </ul>
            <!-- mobile menu -->
            <ul id="slide-out" class="side-nav qt-mobilemenu">
                <li><a href="#home" class="qwsmoothscroll">Home</a></li>
                <li><a href="#photos" class="qwsmoothscroll">Photos</a></li>
                <li><a href="#bio" class="qwsmoothscroll">Bio</a></li>
                <li><a href="#tours" class="qwsmoothscroll">Tours</a></li>
                <li><a href="#booking" class="qwsmoothscroll">Booking</a></li>
            </ul>
            <a href="#" data-activates="slide-out" class="button-collapse qt-menu-switch"><span class="lnr lnr-menu"></span></a>
        </nav>
    </div>
    <!-- ====================== MENU END ================================================ -->


            <!-- main section  ================================ -->
            <main class="qt-main">
                
                <div id="video" class="section section-video  scrollspy" >
                    <div class="qt-framed flow-text"  data-100p-top="opacity:0;" data-80p-top="opacity:0;" data-30p-top="opacity:1;" >
                        <h2 class="qt-section-title">Promo Video 2017<i class="deco"></i></h2>
                        <div class="content">
                            <!-- <div class="qtvideos"> -->
                                <iframe width="1000" height="800" src="https://www.youtube.com/embed/rio7mPaVKWM" frameborder="0" allowfullscreen></iframe>
                                <!-- <div class="center-align"><a href="https://www.youtube.com/watch?v=rio7mPaVKWM" class="swipebox"><img src="images/video1.jpg" alt="DJ DW4RF Promo Video 2017"><span class="lnr lnr-film-play"></span></a><h5>DJ DW4RF Promo Video 2017</h5></div> -->

                            <!-- </div> -->
                        </div>
                    </div>
                </div>
                <!-- ====================== SECTION PHOTOS ================================================ -->
                <div id="photos" class="section section-photos black scrollspy">
                    <h2 class="qt-section-title" data-100p-top="opacity:0;" data-80p-top="opacity:0;" data-50p-top="opacity:1;">PHOTOS<i class="deco"></i></h2>
                    <div class="qtgallery"  data-bottom-top="opacity:0;" data-center-top="opacity:1;">
                        <div><img src="images/photos-3.jpg" alt="photo"/></div>
                        <div><img src="images/photos-2.jpg" alt="photo"/></div>
                        <div><img src="images/photos-1.jpg" alt="photo"/></div>
                        <div><img src="images/photos-4.jpg" alt="photo"/></div>
                        <div><img src="images/photos-5.jpg" alt="photo"/></div>
                        <div><img src="images/photos-6.jpg" alt="photo"/></div>
                        <div><img src="images/photos-7.jpg" alt="photo"/></div>
                        <div><img src="images/photos-8.jpg" alt="photo"/></div>
                    </div>
                </div>
                <!-- ====================== SECTION PHOTOS END ================================================ -->

                <!-- ====================== SECTION BIOGRAPHY ================================================ -->
                <div id="bio" class="section section-bio parallax-container qt-fullscreen scrollspy" >
                    <div class="parallax" ><img src="images/galaxy-1.jpg" alt="background"></div>
                    <div class="container">
                        <div class="qt-framed flow-text"  data-100p-top="opacity:0;" data-80p-top="opacity:0;" data-30p-top="opacity:1;" >
                            <h2 class="qt-section-title">BIO<i class="deco"></i></h2>
                            <div class="content qt-polydecor poly2">

                                <p>An sich selbst glauben und andere mit Musik glücklich machen – dafür
                                steht DJ DW4RF.</p>

                                <p>Mit 1,23 Meter Körpergröße ist DJ DW4RF – Europas kleinster DJ – aber
                                was manche Leute entmutigt hätte, es in der Showszene zu versuchen, hat
                                diesen Musiker nicht davon abgehalten, seinen Traum zu verwirklichen.
                                Und dasselbe will er auch für andere: „Mein persönliches Ziel als DJ
                                ist, dass ich auch Menschen mit Handicap dazu ermutige, dass sie etwas
                                aus ihrem Leben machen und sich nicht schämen oder sonst was, nur weil
                                sie anders sind.“</p>

                                <p>Seit 2014 heizt DW4RF in Clubs und auf Festivals überall in Österreich
                                ein. Seine Spezialitäten: EDM, Dance, Electronic, Progressive House, Bounce.
                                2016 war DW4RF einer der Headliner für Spring Break Europe.</p>
                                
                                <p>Bisher konntet ihr DJ DW4RF in Locations wie dem Princeton Club, der
                                Livestage Innsbruck, dem Bollwerk Graz oder im Project X Dornbirn
                                erleben. Folgt ihm auf Facebook, um zu erfahren, wo ihr in Zukunft die
                                Chance habt!</p>
                                
                                <p>Hinter dem Pult wird DJ DW4RF zu einem Partyfeger mit Gespür für sein
                                Publikum. Stagedives und Partykanonen bringen die Stimmung zum Kochen,
                                während er zugleich für Fotos mit Fans ganz entspannt zu haben ist.</p>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ====================== SECTION BIOGRAPHY END ================================================ -->

                <!-- ====================== SECTION TOURS ================================================ -->
                <div id="tours" class="section section-video parallax-container qt-fullscreen scrollspy" >
                    <div class="parallax" ><img src="images/photos-1.jpg" alt="background"></div>
                    <div class="container">
                        <div class="qt-framed flow-text"  data-100p-top="opacity:1;" data-80p-top="opacity:0;" data-30p-top="opacity:2;" >
                            <h2 class="qt-section-title">TOURS<i class="deco"></i></h2>
                            <div class="content qt-polydecor poly3">
                            <table class="table table-bordered" width="60%">
                                <thead>
                                    <tr>
                                        <th class="center-align" style="color:#ff1744;">DATE</th>
                                        <th class="center-align" style="color:#ff1744;">LOCATION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- <tr>
                                        <td class="center-align">13.5.2017</td>
                                        <td class="center-align">Salzburg Stadt</td>
                                    </tr>
                                    <tr>
                                        <td class="center-align">24.6.2017</td>
                                        <td class="center-align">Pinzgauer Kanne (SBG)</td>
                                    </tr>
                                    <tr>
                                        <td class="center-align">30.6.2017</td>
                                        <td class="center-align">Tiefgaragen Party (NÖ)</td>
                                    </tr>
                                    <tr>
                                        <td class="center-align">30.6.2017</td>
                                        <td class="center-align">T.B.A</td>
                                    </tr> -->
                                    <tr>
                                        <td class="center-align">7.10.2017</td>
                                        <td class="center-align">Downtown Flachau</td>
                                    </tr>

                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ====================== SECTION TOURS END ================================================ -->

                <!-- ====================== SECTION BOOKING AND CONTACTS ================================================ -->
                <div id="booking" class="section section-booking qt-fullscreen scrollspy  qt-polydecor poly3">
                    <div class="qt-valign-wrapper">
                        <div class="qt-valign flow-text">
                            <h2 class="qt-section-title" data-100p-top="opacity:0;" data-70p-top="opacity:1;">BOOKING<i class="deco"></i></h2>
                            <div class="qt-booking-form">
                                <ul class="tabs">
                                    <li class="tab col s6"><h5><a href="#form" class="active">Booking form</a></h5></li>
                                    <li class="tab col s6"><h5><a href="#contacts">Contacts</a></h5></li>
                                </ul>
                                <div id="form" class="row">
                                    <form class="col s12" method="post" action="{{ url('/book') }}">
                                        {{ csrf_field() }}
                                        <h4 class="center-align">Get in touch!</h4>
                                        <div class="row">
                                            <div class="input-field col s6">
                                              <input name="first_name" id="first_name" type="text" required>
                                              <label for="first_name">First Name</label>
                                            </div>
                                            <div class="input-field col s6">
                                              <input name="last_name" id="last_name" type="text" required>
                                              <label for="last_name">Last Name</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                              <input name="email" id="email" type="email" required>
                                              <label for="email">Email</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                              <input name="contact" id="contact" type="text" required>
                                              <label for="contact">Contact Number</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <textarea name="message" id="message" class="materialize-textarea" maxlength="300" required></textarea>
                                                <label for="message">Message</label>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="input-field col s12">
                                                <p><button class="btn waves-effect waves-light" type="submit" name="action">
                                                    <span class="lnr lnr-rocket"></span> Submit
                                                </button></p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div id="contacts" class="row qt-contacts">
                                    <div class="col s12">
                                        <h4 class="center-align">Our contacts</h4>
                                        <p><span class="lnr lnr-smartphone"></span><span>+43(0)660 65 30 729</span></p>
                                        <p><span class="lnr lnr-pencil"></span><span>office@ks-booking.com</span></p>
                                        <p><span class="lnr lnr-location"></span><span>Fleischmanngasse 7/13 1040 Wien ÖSTERREICH</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ====================== SECTION BOOKING AND CONTACTS END ================================================ -->

            </main>
            <!-- main section end ================================ -->

            <!-- footer section  ================================ -->
            
    <!-- ====================== FOOTER ================================================ -->
    <footer class="qt-footer parallax-container blue-grey-text text-lighten-5">
        <div class="parallax"><img src="images/footer.jpg" alt="background"></div>
        <div class="container center-align"  >
            <h3 class="qt-footertitle"><img src="images/logo-footer.png" alt="DJ DW4RF"></h3>
            <h5>KS Booking, Inh. Konstantin Schmelz, Fleischmanngasse 7/13, Wien, Österreich</h5>

            <p class="qt-social">
                <a href="http://www.mixcloud.com/djdw4rf" target="_blank"><span class="qticon-mixcloud"></span></a>
                <a href="https://www.facebook.com/profile.php?id=291386711072310&fref=ts" target="_blank"><span class="qticon-facebook"></span></a>
                <a href="https://www.instagram.com/djdw4rf/" target="_blank"><span class="qticon-instagram"></span></a>
                <a href="https://www.youtube.com/channel/UCaXZFqw0OiucUz9zbuEvyfQ" target="_blank"><span class="qticon-youtube"></span></a>
                <a href="http://www.soundcloud.com/djdw4rf" target="_blank"><span class="qticon-soundcloud"></span></a>
            </p>
            <h5><a href="{{ url('/') }}">Home</a> | <a href="{{ url('/legal') }}">Imprint</a></h5>
        </div>
        <div class="qt-particles" id="particlesfooter" data-color="#ffffff" data-opacity="0.2" data-speed="1"></div>
    </footer>
    <div class="qt-firefoxfix"></div>
    <!-- ====================== FOOTER END ================================================ -->
        <!-- footer section end  ================================ -->

        <!-- QT FOOTER ================================ -->
        <script src="js/modernizr-custom.js"></script>
        <script src="js/jquery.js"></script><!--  JQUERY VERSION MUST MATCH WORDPRESS ACTUAL VERSION (NOW 1.12) -->
        <script src="js/jquery-migrate.min.js"></script><!--  JQUERY VERSION MUST MATCH WORDPRESS ACTUAL VERSION (NOW 1.12) -->

        <!--  CUSTOM JS LIBRARIES: =========================================================== -->
        <script src="js/materializecss/bin/materialize.min.js"></script>
        <script src="components/slick/slick.min.js"></script>
        <script src="components/skrollr/skrollr.min.js"></script>
        <script src="components/particles/particles.min.js"></script>
        
        <script src="components/swipebox/lib/ios-orientationchange-fix.js"></script>
        <script src="components/swipebox/src/js/jquery.swipebox.min.js"></script>
        
        <!-- MAIN JAVASCRIPT FILE ================================ -->
        <script src="js/qt-main.js"></script>

    </body>
</html>